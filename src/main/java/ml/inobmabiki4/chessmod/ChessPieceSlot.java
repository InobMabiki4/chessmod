package ml.inobmabiki4.chessmod;

import net.minecraft.world.Container;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class ChessPieceSlot extends Slot {
    public ChessPieceSlot(Container p_40223_, int p_40224_, int p_40225_, int p_40226_) {
        super(p_40223_, p_40224_, p_40225_, p_40226_);
    }

    @Override
    public boolean mayPlace(ItemStack p_40231_) {
        return p_40231_.getItem() instanceof ChessPieceItem;
    }

    @Override
    public int getMaxStackSize() {
        return 1;
    }
}

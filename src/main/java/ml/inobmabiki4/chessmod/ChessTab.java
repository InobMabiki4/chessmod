package ml.inobmabiki4.chessmod;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ChessTab extends CreativeModeTab {
    public static final ChessTab CHESS_TAB = new ChessTab();

    private ChessTab() {
        super("chessmod");
    }

    @Override
    public ItemStack makeIcon() {
        return new ItemStack(ChessPieceItem.BLACK_KING);
    }
}

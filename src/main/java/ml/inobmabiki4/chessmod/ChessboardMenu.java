package ml.inobmabiki4.chessmod;

import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.world.Container;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class ChessboardMenu extends AbstractContainerMenu {
    public static final MenuType<ChessboardMenu> CHESSBOARD_MENU_TYPE = new MenuType<>(ChessboardMenu::new);
    private final Container chessboard;
    static {
        MenuScreens.register(ChessboardMenu.CHESSBOARD_MENU_TYPE, ChessboardScreen::new);
    }

    public ChessboardMenu(int p_39433_, Inventory p_39434_) {
        this(p_39433_, p_39434_, new SimpleContainer(64));
    }

    public ChessboardMenu(int p_39436_, Inventory p_39437_, Container p_39438_) {
        super(CHESSBOARD_MENU_TYPE, p_39436_);
        checkContainerSize(p_39438_, 64);
        this.chessboard = p_39438_;
        p_39438_.startOpen(p_39437_.player);
        int i = 72;

        for (int j = 0; j < 8; ++j) {
            for(int k = 0; k < 8; ++k) {
                this.addSlot(new ChessPieceSlot(p_39438_, k + j * 8, 17 + k * 18, 18 + j * 18));
            }
        }

        for (int l = 0; l < 3; ++l) {
            for(int j1 = 0; j1 < 9; ++j1) {
                this.addSlot(new Slot(p_39437_, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + i));
            }
        }

        for (int i1 = 0; i1 < 9; ++i1) {
            this.addSlot(new Slot(p_39437_, i1, 8 + i1 * 18, 161 + i));
        }
    }

    public ItemStack quickMoveStack(Player p_39253_, int p_39254_) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.slots.get(p_39254_);
        if (slot != null && slot.hasItem()) {
            ItemStack itemstack1 = slot.getItem();
            itemstack = itemstack1.copy();
            if (p_39254_ < 64) {
                if (!this.moveItemStackTo(itemstack1, 64, this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.moveItemStackTo(itemstack1, 0, 64, false)) {
                return ItemStack.EMPTY;
            }

            if (itemstack1.isEmpty()) {
                slot.set(ItemStack.EMPTY);
            } else {
                slot.setChanged();
            }
        }

        return itemstack;
    }

    public boolean stillValid(Player p_39440_) {
        return this.chessboard.stillValid(p_39440_);
    }

    public void removed(Player p_39442_) {
        super.removed(p_39442_);
        this.chessboard.stopOpen(p_39442_);
    }
}

package ml.inobmabiki4.chessmod;

import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.ContainerOpenersCounter;
import net.minecraft.world.level.block.entity.RandomizableContainerBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class ChessboardBlockEntity extends RandomizableContainerBlockEntity {
    public static final BlockEntityType<ChessboardBlockEntity> CHESSBOARD_ENTITY = BlockEntityType.Builder
            .of(ChessboardBlockEntity::new, ChessboardBlock.CHESSBOARD_BLOCK).build(null);

    private NonNullList<ItemStack> items = NonNullList.withSize(64, ItemStack.EMPTY);
    private ContainerOpenersCounter openersCounter = new ContainerOpenersCounter() {
        protected void onOpen(Level p_155062_, BlockPos p_155063_, BlockState p_155064_) {}

        protected void onClose(Level p_155072_, BlockPos p_155073_, BlockState p_155074_) {}

        protected void openerCountChanged(Level p_155066_, BlockPos p_155067_, BlockState p_155068_, int p_155069_, int p_155070_) {}

        protected boolean isOwnContainer(Player p_155060_) {
            if (p_155060_.containerMenu instanceof ChestMenu) {
                Container container = ((ChestMenu) p_155060_.containerMenu).getContainer();
                return container == ChessboardBlockEntity.this;
            } else {
                return false;
            }
        }
    };

    public ChessboardBlockEntity(BlockPos p_155052_, BlockState p_155053_) {
        super(CHESSBOARD_ENTITY, p_155052_, p_155053_);
    }

    public CompoundTag save(CompoundTag p_58612_) {
        super.save(p_58612_);
        if (!this.trySaveLootTable(p_58612_)) {
            ContainerHelper.saveAllItems(p_58612_, this.items);
        }
        return p_58612_;
    }

    public void load(CompoundTag p_155055_) {
        super.load(p_155055_);
        this.items = NonNullList.withSize(this.getContainerSize(), ItemStack.EMPTY);
        if (!this.tryLoadLootTable(p_155055_)) {
            ContainerHelper.loadAllItems(p_155055_, this.items);
        }
    }

    public int getContainerSize() {
        return 64;
    }

    protected NonNullList<ItemStack> getItems() {
        return this.items;
    }

    protected void setItems(NonNullList<ItemStack> p_58610_) {
        this.items = p_58610_;
    }

    protected Component getDefaultName() {
        return new TranslatableComponent("container.chessboard");
    }

    protected AbstractContainerMenu createMenu(int p_58598_, Inventory p_58599_) {
        return new ChessboardMenu(p_58598_, p_58599_, this);
    }

    public void startOpen(Player p_58616_) {
        if (!this.remove && !p_58616_.isSpectator()) {
            this.openersCounter.incrementOpeners(p_58616_, this.getLevel(), this.getBlockPos(), this.getBlockState());
        }
    }

    public void stopOpen(Player p_58614_) {
        if (!this.remove && !p_58614_.isSpectator()) {
            this.openersCounter.decrementOpeners(p_58614_, this.getLevel(), this.getBlockPos(), this.getBlockState());
        }
    }

    public void recheckOpen() {
        if (!this.remove) {
            this.openersCounter.recheckOpeners(this.getLevel(), this.getBlockPos(), this.getBlockState());
        }
    }
}
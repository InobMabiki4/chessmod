package ml.inobmabiki4.chessmod;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;

import javax.annotation.Nullable;

public class ChessPieceItem extends Item {
    public static final ChessPieceItem WHITE_KING = new ChessPieceItem();
    public static final ChessPieceItem WHITE_QUEEN = new ChessPieceItem();
    public static final ChessPieceItem WHITE_BISHOP = new ChessPieceItem();
    public static final ChessPieceItem WHITE_KNIGHT = new ChessPieceItem();
    public static final ChessPieceItem WHITE_ROOK = new ChessPieceItem();
    public static final ChessPieceItem WHITE_PAWN = new ChessPieceItem();
    public static final ChessPieceItem BLACK_KING = new ChessPieceItem();
    public static final ChessPieceItem BLACK_QUEEN = new ChessPieceItem();
    public static final ChessPieceItem BLACK_BISHOP = new ChessPieceItem();
    public static final ChessPieceItem BLACK_KNIGHT = new ChessPieceItem();
    public static final ChessPieceItem BLACK_ROOK = new ChessPieceItem();
    public static final ChessPieceItem BLACK_PAWN = new ChessPieceItem();

    public ChessPieceItem() {
        super(new Properties().tab(ChessTab.CHESS_TAB));
    }

    @Override
    public int getBurnTime(ItemStack itemStack, @Nullable RecipeType<?> recipeType) {
        return 100;
    }
}

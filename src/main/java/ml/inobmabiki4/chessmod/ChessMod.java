package ml.inobmabiki4.chessmod;

import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fmlserverevents.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("chessmod")
public class ChessMod
{
    private static final Logger LOGGER = LogManager.getLogger();

    public ChessMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {}

    private void enqueueIMC(final InterModEnqueueEvent event) {}

    private void processIMC(final InterModProcessEvent event) {}

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {}

    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(
                final RegistryEvent.Register<Block> blockRegistryEvent
        ) {
            final IForgeRegistry<Block> blockRegistry = blockRegistryEvent.getRegistry();
            blockRegistry.register(ChessboardBlock.CHESSBOARD_BLOCK.setRegistryName("chessboard"));
        }

        @SubscribeEvent
        public static void onItemsRegistry(
                final RegistryEvent.Register<Item> itemRegistryEvent
        ) {
            final IForgeRegistry<Item> itemRegistry = itemRegistryEvent.getRegistry();
            itemRegistry.register(
                    ChessboardItem.CHESSBOARD_ITEM.setRegistryName("chessboard")
            );
            itemRegistry.registerAll(
                    ChessPieceItem.WHITE_KING.setRegistryName("white_king"),
                    ChessPieceItem.WHITE_QUEEN.setRegistryName("white_queen"),
                    ChessPieceItem.WHITE_BISHOP.setRegistryName("white_bishop"),
                    ChessPieceItem.WHITE_KNIGHT.setRegistryName("white_knight"),
                    ChessPieceItem.WHITE_ROOK.setRegistryName("white_rook"),
                    ChessPieceItem.WHITE_PAWN.setRegistryName("white_pawn"),
                    ChessPieceItem.BLACK_KING.setRegistryName("black_king"),
                    ChessPieceItem.BLACK_QUEEN.setRegistryName("black_queen"),
                    ChessPieceItem.BLACK_BISHOP.setRegistryName("black_bishop"),
                    ChessPieceItem.BLACK_KNIGHT.setRegistryName("black_knight"),
                    ChessPieceItem.BLACK_ROOK.setRegistryName("black_rook"),
                    ChessPieceItem.BLACK_PAWN.setRegistryName("black_pawn")
            );
        }

        @SubscribeEvent
        public static void onBlockEntityTypesRegistry(
                final RegistryEvent.Register<BlockEntityType<?>> blockEntityTypeRegistryEvent
        ) {
            final IForgeRegistry<BlockEntityType<?>> itemRegistry = blockEntityTypeRegistryEvent.getRegistry();
            itemRegistry.register(ChessboardBlockEntity.CHESSBOARD_ENTITY.setRegistryName("chessboard"));
        }

        @SubscribeEvent
        public static void onMenuTypesRegistry(
                final RegistryEvent.Register<MenuType<?>> menuTypeRegistryEvent
        ) {
            final IForgeRegistry<MenuType<?>> menuTypeRegistry = menuTypeRegistryEvent.getRegistry();
            menuTypeRegistry.register(ChessboardMenu.CHESSBOARD_MENU_TYPE.setRegistryName("chessboard"));
        }
    }
}

package ml.inobmabiki4.chessmod;

import net.minecraft.world.item.BlockItem;

public class ChessboardItem extends BlockItem {
    public static final ChessboardItem CHESSBOARD_ITEM = new ChessboardItem();

    public ChessboardItem() {
        super(
            ChessboardBlock.CHESSBOARD_BLOCK,
            new Properties().tab(ChessTab.CHESS_TAB));
    }
}
